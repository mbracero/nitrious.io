'use strict'

var Logger = require('devnull');
var logger = new Logger({namespacing : 0});

/*
 * GET users listing.
 */
var result = {
  retStatus : 'sucess',
  users: [
    {
      name: 'Peter',
      surname: 'Jhonson',
      city: 'London'
    },
    {
      name: 'Josh',
      surname: 'Pinches',
      city: 'Liverpool'
    },
    {
      name: 'Paul',
      surname: 'Crish',
      city: 'Manchester'
    }
  ]
};

var list = function(req, res){
  logger.log('Serving request for url [GET] ' + req.route.path);
  res.json(result);  
};

module.exports = function(app) {
  app.get('/users', list);
}

'use strict'

var Logger = require('devnull');
var logger = new Logger({namespacing : 0});

/*
 * GET home page.
 */
var index = function(req, res){
  logger.log('Serving request for url [GET] ' + req.route.path);
  res.render('index', { title: req.i18n.__("Hello") });
};

module.exports = function(app) {
  app.get('/', index);
}

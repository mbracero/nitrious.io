'use strict'

var express = require('express')
  , common = require('./common')
  , config = common.config();

module.exports = function(app){
  // all environments
  app.set('title', 'My Application');
  
  // Depend on the environment
  app.set('appName', config.appName);
  
  // development only
  if ('development' == app.get('env')) {
    console.log('DEVVVVVVVVVVVVVVVVVVVVVV');
    app.use(express.errorHandler());
  }
  
  // taging only
  if ('taging' == app.get('env')) {
    console.log('TAGINGGGGGGGGGGGGGG');
  }
  
  // production only
  if ('production' == app.get('env')) {
    console.log('PRODUCTIONNNNNNNNNNNNN');
  }
};
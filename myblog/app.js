
/**
 * Module dependencies.
 */
var express = require('express')
  , http = require('http')
  , path = require('path')
  , I18n = require('i18n-2');

var app = express();



var optionsI18n = {
  // setup some locales - other locales default to en silently
  locales: ['es', 'en'], // EL DEFAULT LOCALE SE DEFINE COMO PRIMER locale
  defaultLocale: 'en', // <====== NO VALE PA NA
  query: 'false',
  directory: __dirname + '/locales'
};





// Sessions
app.use(express.cookieParser());
app.use(express.session({ secret: 'secret string' }));







// Attach the i18n property to the express request object
// And attach helper methods for use in templates
I18n.expressBind(app, optionsI18n);

var url = require("url");
var querystring = require("querystring");

// Custom Middleware
app.use(function(req, res, next) {
	I18n.expressBind(app, optionsI18n);
  
  var locale = 'es';
  
  // Session
  var sess = req.session;
  console.log('sess.views = ' + sess.views);
  if (sess.views) {
    console.log('sess.views % 2 = ' + (sess.views % 2));
    if ( (sess.views % 2) === 0) {
      console.log('ESSSSSSSSSSSSSSSSSSSS');
      locale = 'es';
    } else {
      console.log('ENNNNNNNNNNNNNNNNNNNN');
      locale = 'en';
    }
    sess.views++;
  } else {
    sess.views = 1;
  }
  
  //console.log('A req.getLocale = ' + locale.red);
  //console.log('A req.getLocale = ' + req.i18n.getLocale().green);
  req.i18n.setLocale(locale); // Si no lo encuentra va a defaultLocale
  //console.log('D req.getLocale = ' + locale.yellow);
  //console.log('D req.getLocale = ' + req.i18n.getLocale().blue);
  //req.i18n.setLocaleFromQuery();
	next();
});




// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.bodyParser());
app.use(express.methodOverride()); // The express.methodOverride middleware alters the HTTP method to whatever is given as value in this hidden field
app.use(app.router);

//app.get('/', routes.index);
require('./routes')(app);
//app.get('/users', user.list);
require('./routes/user')(app);

// config
require('./config')(app);
console.log('======================> ' + app.get('appName') );
console.log('%%%%%%%%%%%%%%%%%%%%%%> ' + app.get('title') );

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

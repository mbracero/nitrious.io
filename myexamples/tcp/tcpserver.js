var net = require('net');

global.hola = 'Hello';

var server = net.createServer(function(conn) {
  console.log('connected');
  
  conn.on('data', function (data) {
    console.log(data + ' from ' + conn.remoteAddress + ' ' + conn.remotePort);
    console.log('global :: ' + global.hola);
    conn.write('Repeating: ' + data);
  });
            
  conn.on('end', function() {
    console.log('Client closed connection');
  });
  
}).listen(8124);

console.log('Listening on port 8124');
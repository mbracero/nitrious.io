function outer() {
  var a = 1;
  
  function inner() {
    /* NOTHING */
  }
  
  var b = 1;
  
  if (a!==1) {
    var c = 2;
  }
  
  console.log('a.- ' + a);
  console.log('b.- ' + b);
  console.log('c.- ' + c);
}

outer();

function MainHandler() {
  this.displayFake = function(req, res, next) {
    "use strict";
    
    console.log("MainHandler.displayFake");
    /*
    res.writeHead(200, {"Content-Type": "text/html"});
    res.write("200 Response\n");
    res.end();
    */
    
    return res.render("login", {username:"", password:"", login_error:""})
    
    //return res.render('home', {title: "homeEEEEeeeEEEeEEeEEEEEEEeeeeeE"});
  }
  
  this.displayJSON = function(req, res, next) {
    "use strict";
    
    console.log("MainHandler.displayJSON");
    
    var _json = {result : 'OK'};
    
    res.json(_json);
  }
}

module.exports = MainHandler;
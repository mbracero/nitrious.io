/**
  Middleware.js
  Funciones que se ejecutan antes que el handler de route.
  Se usa para aniadir funcionalidad a la aplciacion (validar entradas, insertar atributos en sesión, etc...)
**/
var express = require('express');

module.exports = function (app) {
  app.use(express.logger('dev'));

  app.use(express.cookieParser());

  app.use(express.bodyParser());
  
  // Todas las llamadas pasan por esta funcion
  app.use(function (req, res, next) {
    // Aqui por ejemplo podemos hacer accesible el objeto sesion para las cistas
    // res.locals.session = req.session;
    res.locals.check = true;
    next(); // Seguimos con la siguiente coincidencia del route
  });
}

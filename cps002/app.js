var express = require("express");
var http = require("http");
var consolidate = require("consolidate");
var middleware = require('./middleware');
var routes = require("./routes");

var app = express();

// Register our templating engine
// assign the swig engine to .html files
app.engine('html', consolidate.swig);

// set .html as the default extension 
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

// all environments
app.set('port', process.env.PORT || 3000);

middleware(app);
routes(app);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
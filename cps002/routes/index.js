var errors = require("./errors");
var MainHandler = require("../handlers/mainHandler");
var checkAccess = require("../middleware/checkAccess");

var index = function(req, res){
  console.log('Serving request for url [GET] ' + req.route.path);
  
  res.writeHead(200, {"Content-Type": "text/html"});
  res.write("200 Response\n");
  res.end();
};

module.exports = exports = function(app) {
  var mainHandler = new MainHandler();
  
  app.get("/", checkAccess, mainHandler.displayFake);
  
  app.post("/resultJson", mainHandler.displayJSON);

  // Errores por 'not found' o del servidor
  errors(app);
}

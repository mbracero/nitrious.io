/**
  errors.js
  Este fichero lo usamos para gestionar los errores que se puedan dar en la aplicacion.
  Siempre lo colocaremos como ultima entrada del route.
**/
module.exports = function(app) {
  //404
  app.use(function (req, res, next) {
    res.status(404);

    if (req.accepts('html')) {
      return res.send('<h2>Lo siento, no existe la página.</h2>');
    }

    if (req.accepts('json')) {
      return res.send({error: 'Not found'});
    }

    res.type('txt');
    res.send('Me parece que te has equivocado');
  });

  // 500
  app.use(function (err, req, res, next){
    console.error('Error en %s\n', req.url, err);
    res.send(500, 'Crack !!!!!!');
  });
}

'user strict'

var users = [
  { id : 1,
  name : "User 1",
  age : 34 }
];

// Displaying users
exports.index = function(req, res) {
  res.send(users);
  //res.render('index', { title: 'Users' });
};

// Returning the HTML form for creating a new user
exports.new = function(req, res) {
  res.send('displaying new user form\n');
};

// Displaying a specific user
exports.show = function(req, res) {
  var indx = parseInt(req.params.id) - 1;
  if (!users[indx])
    res.send('There is no user with id of ' + req.params.id + '\n');
  else
    res.send(users[indx]);
};

// Returning the HTML for editing a specific user
exports.edit = function(req, res) {
  res.send('displaying edit form\n');
}

// Creating a new user
exports.create = function(req, res) {
  var indx = users.length + 1;
  users[users.length] =
    { id : indx,
     name : req.body.username,
     age : parseInt(req.body.userage) };
  
  console.log(users[indx-1]);
  res.send('User ' + req.body.username + ' added with id ' + indx + '\n');
}

// Updating a specific widget
exports.update = function(req, res) {
  var indx = parseInt(req.params.id) - 1;
  users[indx] =
    { id : parseInt(req.params.id),
     name : req.body.username,
     age : parseInt(req.body.userage) };
  
  console.log(users[indx]);
  res.send('Updated ' + req.params.id + '\n');
}

// Deleting a specific widget
exports.delete = function(req, res) {
  var indx = req.params.id - 1;
  delete users[indx];
  console.log('deleted ' + req.params.id);
  res.send('deleted ' + req.params.id + '\n');
}
'use strict'

var controllers = './controllers/';
var usercontroller = require(controllers + 'usercontroller');
var generalcontroller = require(controllers + 'generalcontroller');

module.exports = function(app) {
  var prefix = '/';
  
  /**
   * General
   */
  var generalPrefix = prefix + "";
  app.get(generalPrefix, generalcontroller.index);
  
  /**
   * Users
   */
  var userPrefix = prefix + "users";
  app.get(userPrefix, usercontroller.index); // all
  app.get(userPrefix + '/new', usercontroller.new); // add
  app.get(userPrefix + '/:id', usercontroller.show); // show
  app.get(userPrefix + '/:id/edit', usercontroller.edit); // edit
  app.post(userPrefix + '/create', usercontroller.create); // create
  app.put(userPrefix + '/:id', usercontroller.update); // update
  app.delete(userPrefix + '/:id', usercontroller.delete); // delete
}